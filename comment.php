<?php require_once 'php/connect.php'; require_once 'php/functions.php'; ?>
<!doctype html>
<html>
	<head>
		<title>Chat Room</title>
		<meta charset="UTF-8" lang="en-US">
		<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/style2.css">
		<script src="js/global.js"></script>
	</head>
	<body>
	<a href="index.php" class="btn btn-info btn-lg">Go back</a>
		<div class="page-container">
			<?php 
				get_total();
				require_once 'php/check_com.php';
			?>
			<form action="" method="post" class="main">
				<label>enter a brief comment</label>
				<textarea class="form-text" name="comment" id="comment"></textarea>
				<br />
				<input type="submit" class="form-submit" name="new_comment" value="post">
			</form>
			<?php get_comments(); ?>
		</div>
		
	</body>
</html>
