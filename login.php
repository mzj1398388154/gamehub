<!doctype html>
<?php
  include("checklogin.php");
?>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">
    <title>Sign in for Game Hub</title>
  </head>

  <body class="text-center">
    <form class="form-signin" method="POST" action="redirect.php">
      <img class="mb-4" src="image/logo.png" alt="logo" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Please Sign in</h1>
      <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
      <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <p class="mt-5 mb-3 text-muted">Don't hava an account? Sign up <a href="signup.php">Here</a></p>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
	</body>
	
</html>
