<!doctype html>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signupsuccess.css" rel="stylesheet">
    <title>Sign Up Done!</title>
  </head>

  <body class="text-center">
    <form class="form-signin">
      <img class="mb-4" src="image/logo.png" alt="logo" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Sign Up Done!</h1>
      <a href="login.php">Back to log in</a>
    </form>
	</body>
	
</html>
